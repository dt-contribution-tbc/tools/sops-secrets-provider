import express, { Request, Response } from 'express';
import { exec } from 'child_process';
import { nanoid } from 'nanoid';
import os from 'os';
import path from 'path';
import { writeFileSync } from 'fs';
import yaml from 'js-yaml';

const app = express();
const port = 3000;

app.use(express.json());
app.use(express.raw({ type: 'application/x-yaml' }));

app.get('/', (req: Request, res: Response) => {
    res.send('Hello World!');
});

let data: { [key: string]: any } = {};

app.post('/api/load', async (req: Request, res: Response) => {
    if (req.headers['content-type'] === 'application/x-yaml') {
        try {
            const yamlContent = req.body.toString();
            const tmpFilePath = path.join(os.tmpdir(), nanoid() + '.yml');

            writeFileSync(tmpFilePath, yamlContent);

            
            data = await new Promise((resolve, reject) => {
                exec(`sops -d ${tmpFilePath}`, (error, stdout, stderr) => {
                    if (error) {
                        console.error(`exec error: ${error}`);
                        reject('Error decrypting file');
                        return;
                    }
                    resolve(yaml.load(stdout) || {});
                });
            });
        } catch (e) {
            res.status(500).send('Error parsing YAML');
            return;
        }
        res.send('OK');
    } else {
        res.status(400).send('Bad Request');
    }
});

app.get('/api/secrets', (req: Request, res: Response) => {

    const field = req.query.field as string;

    if (field && data[field]) {
        res.send(data[field]);
    } else if (field && !data[field]) {
        res.status(404).send('Field not found');
    } else {
        res.json(data);
    }
});

app.listen(port, () => {
    console.log(`Express app listening at http://localhost:${port}`);
});