# API for SOPS (Secrets OPerationS)

[Secrets OPerationS (SOPS)](https://getsops.io/) is a simple and flexible tool for managing secrets in Git. It is an open-source project.

SOPS supports multiple formats, including YAML, JSON, ENV, INI, and BINARY. It provides encryption with AWS KMS, GCP KMS, Azure Key Vault, age, and PGP.

## Getting started

SOPS is primarily a CLI tool and does not provide an API out of the box. However, we have added an API to facilitate its use in our pipelines.

## Initiation of the Service

The endpoint */api/load* accepts a sops-encrypted yaml-file and feeds the service for the current run.

```sh
POST /api/load
```

## Endpoint for Accessing Secrets

The endpoint */api/secrets?field=xxx* decrypts the specified field from the secrets file.

``` sh
GET /api/secrets?field=xxxx
```
 
## Example Use Case
Consider a secret file that contains access_key and secret_key for accessing an S3 storage. You can decrypt the access_key field by making the following request:

```sh
GET /api/secrets?field=access_key
```



